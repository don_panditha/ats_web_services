Copyright Footer
================

INTRODUCTION
------------

The Copyright Footer module simply creates a block for Copyright © footer.
You can configure the start year; and the current year is automatically updated.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/copyright_footer

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/copyright_footer

REQUIREMENTS
------------

- N/A

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings.

- Organization name
- Year origin from (Leave blank if not necessary.)
- Year to date (Leave blank then the current year (2018) automatically
  shows up.)
- Version (Leave blank if not necessary.
- Version URL (Leave blank if not necessary. It works w/ the version number
  above. If you don't input the version number, this field will be simply
  ignored.)

MAINTAINERS
-----------

Current maintainers:
 * Yas Nai (yas) - https://www.drupal.org/u/yas
